const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const app = express();
const axios = require('axios')
app.use(bodyParser.json());


// ===== AUTOMATISATION DU POST & DU GET =====
setInterval(()=>{
    axios.post('http://localhost:3000/login', {
        username: 'john',
        password: 'password123admin'
    })
    .then(res=>{
        axios.get('http://localhost:4000/books', {
        headers: {"Authorization" : `Bearer ${res.data.accessToken}`}
        })
        .then(res=>{
            console.log(res.data);
        })
        .catch(err =>{
            console.error(err);
        })
    })
    .catch(err =>{
        console.error(err);
    })
}, 5000, 0)

setInterval(()=>{
    axios.post('http://localhost:3000/login', {
        username: 'john',
        password: 'password123admin'
    })
    .then(res=>{
        axios.post('http://localhost:4000/books', {
            author: `truc${Date.now()}`,
            country: 'LA FRONCE',
            language: 'French',
            pages: `${Date.now()}`,
            title: "bidule",
            year: `${Date.now()}`
        },
        {
            headers: {"Authorization" : `Bearer ${res.data.accessToken}`}
        })
        .then(res=>{
            console.log(res.data)
        })
    })
},5000, 0)


app.listen(5000, () => {
    console.log('Bot service on http://localhost:5000');
});