exercice-jwt-bot

Exercice tutoriel d'une première approche du Json Web Token

Création de microservice api REST d'authentification et de listage de livres.
Création de bot de reception de la liste de livres et d'insertion de livre

PREREQUIS 
NodeJS et npm

MODULES
Dans la racine du dossier, faire :
npm init
npm install express axios body-parser jsonwebtoken

Lancer les scripts via les commandes suivantes:
node index.js
node books.js
node bot.js